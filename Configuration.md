<!--
SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf

SPDX-License-Identifier: CC-PDDC
-->

# Configuration

RISA is executed using a configuration file in JSON format.

## Overall structure of the configuration file
A valid RISA configuration file needs to include at least these keys:
```
{
    "modeOfOperation": "description of the configured pipeline",
    "libraryPaths" : [
        "/path/to/risa-libraries/libRISA_Core.so",
        "/path/to/risa-libraries/libRISA_Template.so",
        ...
        "/path/to/risa-libraries/libRISA_MyLibrary.so"
    ],
    "pipeline" : {
        "runtimeConfigPath" : "/path/to/store/runtime-configuration-files/",
        "Stages" : [
            { < stage descriptor 1 >},
            { < stage descriptor 2 >},
            ...
            { < stage descriptor x >}
        ],
        "Connections" : [
            { < connection descriptor 1 >},
            { < connection descriptor 2 >},
            ...
            { < connection descriptor y >}
        ],
        "Observers" : [
            { < observer descriptor 1 >},
            { < observer descriptor 2 >},
            ...
            { < observer descriptor z>}
        ]
    }
}
```

- The `modeOfOperation` is a general description of the configuration file intent and only required for more meaniningful logging.  
- The `libraryPaths` define an array of all the used RISA libraries in the pipeline. RISA will dynamically load the requested stage implementations from these libraries. More on that in the section on stage descriptors.
- The `pipeline` group describes the overall structure of the pipeline, defined by the uses `Stages`, the `Connections` between the stages and possibly attached `Observers` which handle runtime reconfiguration. For this, the `runTimeConfigPath` must also be provided, which will hold all the runtime configuration files which are monitored for changes during execution.

### Stage descriptor
Every stage descriptor provides some basic information about the stage to use, e.g.
```
{
    "type": "Process",
    "name": "Filter2D",
    "numInputs": 1,
    "numOutputs": 2,
    "identifier": "MyGaussianBlur",
    "parameterSet": "filter_1",
    "library": "RISA_Core"
}
```

*All the fields are mandatory.*  
- `type` describes the general type of stage to load. Valid values are `"Process"`, `"Source"`, `"Sink"`.  
- `name` is the name of the implemented stage class. **This must fit the function name exposed by the respective library!**  
- `numInputs` and `numOutputs` refer to the number of consumed/produced streams respectively. For `Source` stages, these must be `0` and `1`, for `Sink` stages `1` and `0`, respectively.  
- `identifier` is a unique identifier for the stage. This identifier is used to define connections and attached observers.  
- `parameterSet` may be used as a reference to which parameters from the config file to use. E.g. when using multiple filter stages these may use different configurations from the config file.  
- `library` is the name of the RISA library which implements this stage. Note that the file name for the libraries are `libRISA_xxx.so`. For the `library` value, use only `RISA_xxx`, e.g. use `RISA_Core` when referring to a stage implemented in `libRISA_Core.so`.  

### Connection descriptor
Connections between the created stages are defined in connection descpriptors like so:
```
{
    "fromStage": "producingStage",
    "toStage": "consumingStage",
    "toPort": 0
}
```

*All the fields are mandatory.*  
- `fromStage` and `toStage` are the identifiers (defined in the stage descriptors) of the producing and consuming stage, respectively.  
- `toPort` refers to the input port of the consuming stage. Usually `0`, for stages with more than one input all ports from `0` to `numInputs - 1` must be connected. Outgoing connection ports for stages with multiple output ports are connected in order of appearence in the connection descriptors.

### Observer descriptor
`GLADOS` currently only implements `FileObserver` subjects. The observer descriptors are therefore simply defined by:
```
{
    "stage": "identifier",
    "file": "filename"
}
```

*All the fields are mandatory.*  
- `stage` refers to the identifier defined in the stage descriptors.  
- `file` is the file name of the runtime configuration file. An empty file with this name will be created in the `runtimeConfigPath`. Changes to this runtime config file will then be reflected asychronously in the pipeline to, e.g., update stage parameters while the pipeline is executing.

### Settings per stage
The settings for each stage are defined in its respective library. For an example, check the libRISA_Core reference [here](https://gitlab.hzdr.de/fwdf/measurementscience/projects/ufxct/librisa_core/-/blob/main/Configuration.md).
