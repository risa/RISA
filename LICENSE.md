## License

Copyright © 2022 Helmholtz-Zentrum Dresden-Rossendorf

This work is licensed under multiple licenses:
- The source code and the accompanying material are licensed under [Apache-2.0](LICENSES/Apache-2.0.txt).
- Insignificant files are licensed under [CC-PDDC](LICENSES/CC-PDDC.txt).

Please see the individual files for more accurate information.

> **Hint:** We provided the copyright and license information in accordance to the [REUSE Specification 3.0](https://reuse.software/spec/).
