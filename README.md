<!--
SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf

SPDX-License-Identifier: CC-PDDC
-->

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.200363.svg)](https://doi.org/10.5281/zenodo.200363)

# RISA - Real-time Image Stream Algorithms

### Doxygen documentation can be found [here](https://risa-docs.github.io/).

### Description
Real-time image stream algorithms (RISA) is a high-throughput, low-latency image stream processing command-line application. Based on a configuration file, it will construct a processing pipeline of distinct processing steps and process a given image stream. Implementations of such processing steps are loaded from RISA-compatible shared libraries. Basic image manipulation capabilities (filtering, masking, ...) comes out-of-the-box with the [`RISA_Core` library](https://gitlab.hzdr.de/risa/librisa_core). Check [below](#build-your-own-risa-compatible-processing-library) on how to create your own RISA-compatible library!

### Prerequisites:
- cmake (>= 3.19)
- CUDA (>= 11.2) and compatible host compiler (e.g. gcc >= 9)
- Boost (>= 1.71)
- glados (included as gitmodule)

### Installation on Ubuntu 20.04:
- install cmake (if only an older version is installed by default, get the newest one from [here](https://cmake.org/download/)): `sudo apt install cmake`
- install boost: `sudo apt install libboost-all-dev`
- install CUDA (download from NVIDIA homepage: https://developer.nvidia.com/cuda-toolkit and follow the  instructions)
- in order to build the documentation, `doxygen` and `graphviz` need to be installed: `sudo apt install doxygen graphviz`

### Installation on Windows:
- install CUDA (download from NVIDIA homepage: https://developer.nvidia.com/cuda-toolkit and follow the  instructions)
- install [boost](https://www.boost.org/) using a terminal with admin privileges (tested for version 1.78):
```
bootstrap
b2 --build-type=complete install
```
- then add to `PATH`: "C:/Boost/lib"

### Building the software:
```
git clone https://codebase.helmholtz.cloud/risa/RISA 
cd RISA
git submodule update --init --recursive
mkdir ../build && cd ../build
cmake ../RISA
make
```
- By default, the software is built for CUDA compute capability 6.1. To add more, change the last line of `CMakeLists.txt` in `/risa`:  
e.g. `set_property(TARGET risa PROPERTY CUDA_ARCHITECTURES 50)`  
- if build was successful, there is an executable `risa` in the `build/bin` directory
- the template shared library is to be found in `build/lib`
- the doxygen documentation can be found in the `docs/html` folder

### Running the software:
- create a pipeline configuration file as described [here](https://gitlab.hzdr.de/risa/RISA/-/blob/master/Configuration.md)
- the software can be invoked by issuing the following command:
    ```./risa <path_to_configuration_file>```

### Build your own RISA-compatible processing library 
A RISA pipeline consists of three basic types of processing steps: sources, processors and sinks. You can add your own implementation for each of these types by adhering to the [RISAModuleInterface](https://gitlab.hzdr.de/risa/RISA/-/blob/master/risa/include/risa/RISAModuleInterface.h):
- a __source__ stage class must inherit `RISAModule_Source<output_type>` and  implement:
    - `output_type loadImage()`
    - `void update(glados::Subject s)`
- a __processor__ stage class must inherit `RISAModule_Process<input_type, output_type>` and implement:
    - `void process(input_type image, int port)`
    - `output_type wait(int port)`
    - `void update(glados::Subject s)`
- a __sink__ stage class must inherit `RISAModule_Sink<input_type>` and implement:
    - `void saveImage(input_type image)`
    - `void update(glados::Subject s)`

Communication between stages uses memory managed by the underlying `GLADOS` pipelining framework. Input and output types must therefore be based on a `GLADOS` memory manager. The RISA module interface defines these handy templated typedefs:
- `host_array<T>` for data located in host memory. `T` can be any plain old data type.
- `cuda_array<T>` for data located in device memory. Again, `T` can be any plain old data type.

Having implemented the actual algorithms, your library needs to provide them to the `risa` base application. For this, you need to provide 
- a library name string,
- a library version string, 
- a list of strings describing the implemented processor, sink and source stages, and
- a list of disabled stages along with their reason (e.g. a missing library).
 
You can find an example in the template library.

In the implemented function lists, the input/output types must be encoded as such:
- The type `cuda_array<T>` is represented as `"cT"`, e.g. `cuda_array<float>` => `"cfloat"`
- The type `host_array<T>` is represented as `"hT"`, e.g. `host_array<uint32_t>` => `"huint32_t"`

For each of the listed stage classes, a constructor and destructor function must be provided:
- for __source__ stages with name `MySource`:
    - `EXPORT RISAModule_Source<output_type>* create_MySource(const char* configFile, const char* parameterSet)` -> return a `new` instance of `MySource`
    - `EXPORT void destroy_MySource(RISAModule_Source<output_type>)` -> `delete` instance of `MySource`
- for __processor__ stages with name `MyProcessor`:
    - `EXPORT RISAModule_Processor<input_type, output_type>* create_MyProcessor(const char* configFile, const char* parameterSet, const int numInputs, const int numOutputs)` -> return a `new` instance of `MyProcessor`
    - `EXPORT void destroy_MyProcessor(RISAModule_Processor<input_type, output_type>)` -> `delete` instance of `MyProcessor`
- for __sink__ stages with name `MySink`:
    - `EXPORT RISAModule_Sink<input_type>* create_MySink(const char* configFile, const char* parameterSet)` -> return a `new` instance of `MySink`
    - `EXPORT void destroy_MySink(RISAModule_Sink<input_type>)` -> `delete` instance of `MySink`

The `EXPORT` macro provides the OS-specific keywords to make the function loadable by RISA.

Note that the naming scheme is fixed - the constructor _must_ be called `create_X` with `X` being the name defined in the `get_implemented_xxx` function, else `risa` won't be able to load the correct function. The same holds true for `destroy_X`.

Lastly, make sure to link your library against the `singleton` shared library provided by GLADOS. This is required for using its built-in memory pool manager.

Check the [template library](https://codebase.helmholtz.cloud/risa/RISA/-/tree/master/libRISA_Template) for an example on how all of this comes together!

### Related works:
_Real-time data processing for ultrafast X-ray computed tomography using modular CUDA based pipelines_, Windisch, Kelling, et al., CPC, 2023 (https://doi.org/10.1016/j.cpc.2023.108719)

_Parallel Algorithm for Connected-Component Analysis Using CUDA_, Windisch, Kaever, et al., Algorithms, 2023 (https://doi.org/10.3390/a16020080)
