// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef TEMPLATE_SINK_H_
#define TEMPLATE_SINK_H_

#include <glados/Image.h>
#include <glados/Queue.h>
#include <glados/cuda/DeviceMemoryManager.h>
#include <glados/cuda/HostMemoryManager.h>
#include <glados/cuda/Memory.h>
#include <glados/observer/Subject.h>

#include "../RISAModuleInterface.h"

#include <map>
#include <thread>

namespace risa
{
namespace cuda
{

//!	This is a template sink stage. It must inherit RISAModule_Sink of its input type.
class Template_Sink : public RISAModule_Sink<cuda_array<float>>
{
	public:
	using manager_type = glados::cuda::DeviceMemoryManager<float, glados::cuda::async_copy_policy>;
	using input_type = glados::Image<manager_type>;

	public:
	Template_Sink(const std::string& configFile, const std::string& parameterSet);

	//! this function is called, when an image exits the software pipeline
	auto saveImage(input_type image) -> void;

	auto update(glados::Subject* s) -> void;

	protected:
	~Template_Sink();

	private:
	//! Read configuration values from configuration file
	/**
	 * All values needed for setting up the class are read from the config file
	 * in this function.
	 *
	 * @param[in] configFile   path to config file
	 * @param[in] parameterSet   identifier for specific settings within the config file
	 *
	 * @retval  true  configuration options were read successfully
	 * @retval  false configuration options could not be read successfully
	 */
	auto readConfig(const std::string& configFile, const std::string& parameterSet) -> bool;

	int m_imageWidth; //!< image width in pixels
	int m_imageHeight; //!< image height in pixels

	std::string m_outputPath, m_fileName; //!< the path and name of the output file
};
}
}

#endif /* TEMPLATE_SINK_H_ */
