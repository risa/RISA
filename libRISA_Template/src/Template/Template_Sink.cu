// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#include <risaTemplate/ConfigReader/ConfigReader.h>
#include <risaTemplate/Template/Template_Sink.h>

#include <glados/MemoryPool.h>
#include <glados/cuda/Check.h>
#include <glados/observer/FileObserver.h> // one example of possible observer-subject subjects

#include <boost/log/trivial.hpp>

#include <exception>

namespace risa
{
namespace cuda
{

Template_Sink::Template_Sink(const std::string& configFile, const std::string& parameterSet)
{

	if(!readConfig(configFile, parameterSet))
	{
		throw std::runtime_error(
			"risa::cuda::Template_Sink: Configuration file could not be loaded successfully.");
	}
}

Template_Sink::~Template_Sink()
{
	// when Memorypool was used, free memory here
	// glados::MemoryPool<hostManagerType>::instance()->freeMemory(m_memoryPoolIdx);
	// when use of cudaStreams, destroy them here
	// for(auto i = 0; i < m_numberOfDevices; i++){
	//    CHECK(cudaSetDevice(i));
	//    CHECK(cudaStreamDestroy(m_streams[i]));
	// }
}

auto Template_Sink::saveImage(input_type img) -> void
{
	if(img.valid())
	{
		// access raw image data using img.data()

		// sink data, e.g. store on disk
	}
}

auto Template_Sink::update(glados::Subject* s) -> void
{
	// subject may call this function to update stage parameters during runtime

	// cast the subject to specific subject implementation:
	// glados::FileObserver* fo = (glados::FileObserver*)s;

	// access members to update stage stage:
	// BOOST_LOG_TRIVIAL(info) << "risa::cuda::Template: ###### UPDATE ###### -> " << fo->m_memberVariable;
}

auto Template_Sink::readConfig(const std::string& configFile, const std::string& parameterSet) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());
	// e.g. reading the number of pixels in the reconstructed image from the given configuration file
	if(configReader.lookupValue("imageWidth", m_imageWidth)
		&& configReader.lookupValue("imageHeight", m_imageHeight))
		return true;
	else
		return false;
}

}
}
