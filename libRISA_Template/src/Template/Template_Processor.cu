// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#include <risaTemplate/ConfigReader/ConfigReader.h>
#include <risaTemplate/Template/Template_Processor.h>

#include <glados/MemoryPool.h>
#include <glados/cuda/Check.h>
#include <glados/observer/FileObserver.h> // one example of possible observer-subject subjects

#include <boost/log/trivial.hpp>

#include <exception>

namespace risa
{
namespace cuda
{

Template_Processor::Template_Processor(const std::string& configFile, const std::string& parameterSet,
	const int numberOfOutputs, const int numberOfInputs)
{

	if(!readConfig(configFile))
	{
		throw std::runtime_error(
			"risa::cuda::Template: Configuration file could not be loaded successfully.");
	}

	CHECK(cudaGetDeviceCount(&m_numberOfDevices));

	// when MemoryPool is required, register here:
	// m_memoryPoolIdx =
	//       glados::MemoryPool<hostManagerType>::instance()->registerStage(m_memPoolSize,
	//             m_numberOfPixels * m_numberOfPixels);

	// custom streams are necessary, because profiling with nvprof not possible with
	//-default-stream per-thread option
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		cudaStream_t stream;
		CHECK(cudaStreamCreateWithPriority(&stream, cudaStreamNonBlocking, 0));
		m_streams[i] = stream;
	}

	// initialize output streams
	m_results = std::vector<glados::Queue<output_type>>(m_numberOfOutputs);

	// initialize worker threads
	for(auto i = 0; i < m_numberOfDevices; i++)
	{
		m_imgs[i] = std::vector<glados::Queue<input_type>>(m_numberOfInputs);
		m_processorThreads[i] = std::thread{&Template_Processor::processor, this, i};
	}

	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Template: Running " << m_numberOfDevices << " thread(s).";
}

Template_Processor::~Template_Processor()
{
	// when Memorypool was used, free memory here
	// glados::MemoryPool<hostManagerType>::instance()->freeMemory(m_memoryPoolIdx);
	// when use of cudaStreams, destroy them here
	// for(auto i = 0; i < m_numberOfDevices; i++){
	//    CHECK(cudaSetDevice(i));
	//    CHECK(cudaStreamDestroy(m_streams[i]));
	// }
}

auto Template_Processor::process(input_type&& img, int inputIdx) -> void
{
	if(img.valid())
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Template:       (Device " << img.device() << " |  Input "
								 << inputIdx << "): Image " << img.index() << " arrived.";
		m_imgs[img.device()][inputIdx].push(std::move(img));
	}
	else if(inputIdx == 0)
	{
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Template: Received sentinel, finishing.";

		// send sentinel to processor threads and wait 'til they're finished
		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_imgs[i][0].push(input_type());
		}

		for(auto i = 0; i < m_numberOfDevices; i++)
		{
			m_processorThreads[i].join();
		}

		// push sentinel to results for next stage
		for(int i = 0; i < m_numberOfOutputs; i++)
			m_results[i].push(output_type());
		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Template: Finished.";
	}
}

auto Template_Processor::wait(int outputIdx) -> output_type { return m_results[outputIdx].take(); }

auto Template_Processor::processor(const int deviceID) -> void
{
	CHECK(cudaSetDevice(deviceID));
	BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Template: Running thread for device " << deviceID;
	while(true)
	{
		auto img = m_imgs[deviceID][0].take();
		if(!img.valid())
		{
			break;
		}

		// if necessary, request memory from MemoryPool here
		auto ret = glados::MemoryPool<manager_type>::getInstance().requestMemory(m_memoryPoolIdx);

		//<-- do work here -->

		// in case of a CUDA stage, synchronization needs to be done here
		// CHECK(cudaStreamSynchronize(m_streams[deviceID]));

		// wait until work on device is finished
		m_results[0].push(std::move(ret));

		BOOST_LOG_TRIVIAL(debug) << "risa::cuda::Template:       (Device " << deviceID << " | Output " << 0
								 << "): Image " << ret.index() << " processed.";
	}
}

auto Template_Processor::update(glados::Subject* s) -> void
{
	// subject may call this function to update stage parameters during runtime

	// cast the subject to specific subject implementation:
	// glados::FileObserver* fo = (glados::FileObserver*)s;

	// access members to update stage stage:
	// BOOST_LOG_TRIVIAL(info) << "risa::cuda::Template: ###### UPDATE ###### -> " << fo->m_memberVariable;
}

auto Template_Processor::readConfig(const std::string& configFile) -> bool
{
	ConfigReader configReader = ConfigReader(configFile.data());
	// e.g. reading the number of pixels in the reconstructed image from the given configuration file
	if(configReader.lookupValue("numberOfPixels", m_numberOfPixels))
		return true;
	else
		return false;
}

}
}
