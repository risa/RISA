// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#include <iostream>
#include <string>
#include <vector>

#include "../include/risaTemplate/RISAModuleInterface.h"
#include "../include/risaTemplate/libRISA_Template.h"

//**********************
// LIBRARY INFORMATION *
//**********************
// clang-format off

// LIBRARY NAME
const std::string libName = "RISA_Template";

// LIBRARY VERSION
#ifndef NDEBUG
const std::string libVers = "DEBUG    " + std::string(__DATE__) + "|" + std::string(__TIME__);
#else
const std::string libVers = "RELEASE  " + std::string(__DATE__) + "|" + std::string(__TIME__);
#endif

// PROCESSOR STAGES IMPLEMENTED IN THIS LIBRARY
const std::vector<std::vector<std::string>> libProcessorStages = {
	{"Template_Processor", "cfloat", "cfloat"}
	/* add further implemented processors here, e.g.:
	{"Template_Processor_2, "cuint16", "hfloat"},
	{"Template_Processor_3, "cuint32", "hfloat"},
	...
	*/
};

// SORUCE STAGES IMPLEMENTED IN THIS LIBRARY
const std::vector<std::vector<std::string>> libSourceStages = {
	{"Template_Source", "cfloat"}
	/* add further implemented sources here, e.g.:
	{"Template_Source_2, "cuint16"},
	{"Template_Source_3, "hfloat"},
	...
	*/
};

// SINK STAGES IMPLEMENTED IN THIS LIBRARY
const std::vector<std::vector<std::string>> libSinkStages = {
	{"Template_Sink", "hfloat"}
	/* add further implemented sources here, e.g.:
	{"Template_Sink_2, "cuint16"},
	{"Template_Sink_3, "hfloat"},
	...
	*/
};

// LIST OF DISABLED STAGES DUE TO MISSING PREREQUISITS
const std::vector<std::vector<std::string>> libDisabledStages = {
	/* add a list of disabled stages with an info string on why it was disabled */
#ifndef FEATURE_FLAG
	{"DisabledStageName", "FEATURE_FLAG was not set"},
#endif
};


// Library information getters - DO NOT MODIFY
EXPORT auto get_library_name(char* ptr) -> uint32_t { return get_str(ptr, libName); }
EXPORT auto get_library_version(char* ptr) -> uint32_t  { return get_str(ptr, libVers); }
EXPORT auto get_implemented_processors(char* ptr, int32_t n, int32_t part) -> uint32_t { return get_str(ptr, n, part, libProcessorStages); }
EXPORT auto get_implemented_sources(char* ptr, int32_t n, int32_t part) -> uint32_t { return get_str(ptr, n, part, libSourceStages); }
EXPORT auto get_implemented_sinks(char* ptr, int32_t n, int32_t part) -> uint32_t {	return get_str(ptr, n, part, libSinkStages); }
EXPORT auto get_disabled_stages(char* ptr, int32_t n, int32_t part) -> uint32_t {	return get_str(ptr, n, part, libDisabledStages); }

// clang-format on

//*****************************
// STAGE CREATORS / DESTROYERS
//*****************************

//***************
// SOURCE STAGES
//***************

// Template_Source
//! Factory returning a new instance of the implenented stage class.
/** The name of this function m__must fit its definition in the `get_implemented_sources` function preprended
 *with `create_`.
 ** E.g., to create a stage `MyStage` this function must be named `create_MyStage`.
 *
 *  @param[in] configFile Path to the configuration file.
 *  @param[in] parameterSet Identifier of paramters for this particular stage instance.
 *  @retval  Pointer to instance of implemented class.
 *
 */
EXPORT auto create_Template_Source(const char* configFile, const char* parameterSet)
	-> RISAModule_Source<cuda_array<float>>*
{
	return new risa::cuda::Template_Source(configFile, parameterSet);
}
//! Destructor for implemented stage instance.
/** The name of this function m__must fit its definition in the `get_implemented_sources` function preprended
 *with `destroy_`.
 ** E.g., to deconstruct a stage `MyStage` this function must be named `destroy_MyStage`.
 *
 *  @param[in] rm Pointer to instance of implemented class.
 *
 */
EXPORT auto destroy_Template_Source(RISAModule_Source<cuda_array<float>>* ptr) -> void
{
	delete ptr; // NOLINT(cppcoreguidelines-owning-memory)
}

//******************
// PROCESSOR STAGES
//******************

// Template_Processor
//! Factory returning a new instance of the implenented stage class.
/** The name of this function m__must fit its definition in the `get_implemented_processors` function
 *preprended with `create_`.
 ** E.g., to create a stage `MyStage` this function must be named `create_MyStage`.
 *
 *  @param[in] configFile Path to the configuration file.
 *  @param[in] parameterSet Identifier of paramters for this particular stage instance.
 *  @param[in] numberOfInputs The total number of input ports of this stage (i.e. number of consumed streams).
 *  @param[in] numberOfOutputs The total number of output ports of this stage (i.e. number of produced
 *streams).
 *  @retval  Pointer to instance of implemented class.
 *
 */
EXPORT auto create_Template_Processor(
	const char* configFile, const char* parameterSet, const int numberOfOutputs, const int numberOfInputs)
	-> RISAModule_Processor<cuda_array<float>, cuda_array<float>>*
{
	return new risa::cuda::Template_Processor(configFile, parameterSet, numberOfOutputs, numberOfInputs);
}
//! Destructor for implemented stage instance.
/** The name of this function m__must fit its definition in the `get_implemented_processors` function
 *preprended with `destroy_`.
 ** E.g., to deconstruct a stage `MyStage` this function must be named `destroy_MyStage`.
 *
 *  @param[in] ptr Pointer to instance of implemented class.
 *
 */
EXPORT auto destroy_Template_Processor(RISAModule_Processor<cuda_array<float>, cuda_array<float>>* ptr)
	-> void
{
	delete ptr; // NOLINT(cppcoreguidelines-owning-memory)
}

//*************
// SINK STAGES
//*************

// Template_Sink
//! Factory returning a new instance of the implenented stage class.
/** The name of this function m__must fit its definition in the `get_implemented_sinks` function preprended
 *with `create_`.
 ** E.g., to create a stage `MyStage` this function must be named `create_MyStage`.
 *
 *  @param[in] configFile Path to the configuration file.
 *  @param[in] parameterSet Identifier of paramters for this particular stage instance.
 *  @retval  Pointer to instance of implemented class.
 *
 */
EXPORT auto create_Template_Sink(const char* configFile, const char* parameterSet)
	-> RISAModule_Sink<cuda_array<float>>*
{
	return new risa::cuda::Template_Sink(configFile, parameterSet);
}
//! Destructor for implemented stage instance.
/** The name of this function m__must fit its definition in the `get_implemented_sinks` function preprended
 *with `destroy_`.
 ** E.g., to deconstruct a stage `MyStage` this function must be named `destroy_MyStage`.
 *
 *  @param[in] ptr Pointer to instance of implemented class.
 *
 */
EXPORT auto destroy_Template_Sink(RISAModule_Sink<cuda_array<uint16_t>>* ptr) -> void
{
	delete ptr; // NOLINT(cppcoreguidelines-owning-memory)
}
