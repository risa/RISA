Updating the docker image:
- `sudo docker run -it registry.hzdr.de/.../containerName`
- apply any changes necessary, then `exit`
- `sudo docker ps -a` to get a commit id of the latest changes
- `sudo docker commit <id> registry.hdr.de/.../containerName` (may take a moment)

To upload it, one has to login:
- `sudo docker login registry.hzdr.de -u <username>`

Then push the changes to the registry:
- `sudo docker push registry.hdr.de/.../containerName`