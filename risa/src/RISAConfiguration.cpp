// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#include <risa/ConfigReader.h>
#include <risa/RISAConfiguration.h>

#include <glados/Utils.h> //FORMAT_PATH

namespace risa
{

RISAConfiguration::~RISAConfiguration()
{
	cleanupPipeline();
	for(size_t i = 0; i < m_libHandles.size(); ++i)
	{
		BOOST_LOG_TRIVIAL(debug) << "Closing libHandle for " << m_libHandleNames[i];
		dlclose(m_libHandles[i]);
	}
}

auto RISAConfiguration::cleanupPipeline() -> void { m_pipeline.reset(); }

auto RISAConfiguration::generateConfiguration(std::string& configFile) -> bool
{
	// LAMBDA FUNCTIONS
	//
	// These function use a C-interface and will throw a few warnings in clang-tidy. We therefore explicitly
	// disable the linter in this section. NOLINTBEGIN
	auto getStrArrayFromLibraryFunction = [](void* libHandle, const std::string& fctnName,
											  std::vector<std::vector<std::string>>& ret,
											  bool mayFail = false) -> bool
	{
		using libFunc_t = uint32_t(char*, int32_t, int32_t);

		dlerror();
		libFunc_t* get_lib_str{nullptr};
		try
		{
			get_lib_str = dlsym_s<libFunc_t>(libHandle, fctnName.c_str());
		}
		catch(...)
		{
			if(!mayFail)
			{
				throw;
			}
			return false;
		}
		const char* dlsym_error = dlerror();

		if(dlsym_error)
		{
			if(!mayFail)
			{
				BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Error when trying to load funtion '"
										 << fctnName << "': " << std::string(dlsym_error);
			}
			dlclose(libHandle);
			return false;
		}
		auto nStrs = (int32_t)get_lib_str(nullptr, -1, -1);
		for(auto iStr = 0; iStr < nStrs; iStr++)
		{
			std::vector<std::string> curr;
			auto nStrParts = (int32_t)get_lib_str(nullptr, iStr, -1);
			for(auto strPart = 0; strPart < nStrParts; strPart++)
			{
				auto strPartSz = (int32_t)get_lib_str(nullptr, iStr, strPart);
				auto* libStr = (char*)malloc((size_t)strPartSz + 1ul);
				if(libStr)
				{
					get_lib_str(libStr, iStr, strPart);
					libStr[strPartSz] = '\0';
					curr.push_back(std::string(libStr));
					free(libStr);
				}
				else
				{
					return false;
				}
			}
			ret.push_back(curr);
		}
		return true;
	};

	auto getStrFromLibraryFunction = [](void* libHandle, std::string fctnName) -> std::string
	{
		using libFunc_t = uint32_t(char*);

		dlerror();
		libFunc_t* get_lib_str = dlsym_s<libFunc_t>(libHandle, fctnName.c_str());

		const char* dlsym_error = dlerror();

		if(dlsym_error)
		{
			BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Error when trying to load funtion '" << fctnName
									 << "': " << std::string(dlsym_error);
			dlclose(libHandle);
			return std::string();
		}

		uint32_t libStrSz = get_lib_str(nullptr);
		char* libStr = (char*)malloc((size_t)libStrSz + 1ul);
		if(libStr)
		{
			get_lib_str(libStr);
			libStr[libStrSz] = '\0';
			std::string ret(libStr);
			free(libStr);
			return ret;
		}
		return std::string();
	};
	// NOLINTEND

	auto printStagesInLibrary =
		[&](std::string libName,
			const std::map<std::string, std::tuple<std::vector<std::string>, void*>>& dict, std::string type,
			const std::string& logLevel = "debug")
	{
		auto padStr = [](std::string str)
		{
			const int typeWidth = 12;
			if(str.size() < typeWidth)
			{
				return str + std::string(typeWidth - str.size(), ' ');
			}
			return str;
		};
		if(logLevel == "debug")
		{
			BOOST_LOG_TRIVIAL(debug) << "Available " << type << " stages in " << libName << ": ";
		}
		else
		{
			BOOST_LOG_TRIVIAL(warning) << "Available " << type << " stages in " << libName << ": ";
		}

		bool foundAny{false};
		for(const auto& [key, val] : dict)
		{
			if(key.starts_with(libName))
			{
				foundAny = true;
				const std::string stageName = key.substr(libName.size() + 1);
				std::string input;
				std::string output;
				auto ioNames = std::get<0>(val);
				if(type == "source")
				{
					input = padStr("---");
					output = padStr(ioNames[0]);
				}
				if(type == "sink")
				{
					input = padStr(ioNames[0]);
					output = padStr("---");
				}
				if(type == "process")
				{
					input = padStr(ioNames[0]);
					output = padStr(ioNames[1]);
				}

				if(logLevel == "debug")
				{
					BOOST_LOG_TRIVIAL(debug) << "[ " << input << " => " << output << "]: " << stageName;
				}
				else
				{
					BOOST_LOG_TRIVIAL(warning) << "[ " << input << " => " << output << "]: " << stageName;
				}
			}
		}
		if(!foundAny)
		{
			if(logLevel == "debug")
			{
				BOOST_LOG_TRIVIAL(debug) << "No " << type << " stages available.";
			}
			else
			{
				BOOST_LOG_TRIVIAL(warning) << "No " << type << " stages available.";
			}
		}
	};

	// FUNCTION ENTRY

	BOOST_LOG_TRIVIAL(info) << "RISAConfiguration: Starting to generate pipeline configuration.";

	m_pipeline = std::make_unique<glados::pipeline::Pipeline>();
	auto cfg = risa::ConfigReader(configFile.data(), false);

	// load dynamic libraries with stage implementations
	std::vector<std::string> libraryPaths;
	cfg.lookupValue<std::string>("libraryPaths", libraryPaths);

	// make sure there are no duplicate loads
	std::set<std::string> uniqueLibraryPaths;
	for(const auto& libraryPath : libraryPaths)
	{
		uniqueLibraryPaths.insert(libraryPath);
	}

	std::map<std::string, std::tuple<std::vector<std::string>, void*>> sourceDict;
	std::map<std::string, std::tuple<std::vector<std::string>, void*>> processorDict;
	std::map<std::string, std::tuple<std::vector<std::string>, void*>> sinkDict;

	auto printStageHint = [&](std::string stageName)
	{
		// check if the stage was disabled
		for(size_t i = 0; i < m_libHandles.size(); i++)
		{
			std::vector<std::vector<std::string>> disabledStages;
			if(getStrArrayFromLibraryFunction(m_libHandles[i], "get_disabled_stages", disabledStages, true))
			{
				for(const auto& disabledStage : disabledStages)
				{
					if(disabledStage.size() > 0 && disabledStage[0].compare(stageName) == 0)
					{
						BOOST_LOG_TRIVIAL(warning)
							<< "RISAConfiguration: Stage '" << stageName << "' was disabled in "
							<< m_libHandleNames[i] << " because: " << disabledStage[1];
					}
				}
			}
		}

		// check if the stage may be availble in another library
		for(const auto& [key, val] : sourceDict)
		{
			if(key.ends_with(stageName))
			{
				BOOST_LOG_TRIVIAL(warning)
					<< "RISAConfiguration: Did you mean to use the source stage '" << stageName
					<< "' in library '" << key.substr(0, key.size() - stageName.size() - 1) << "'?";
			}
		}
		for(const auto& [key, val] : processorDict)
		{
			if(key.ends_with(stageName))
			{
				BOOST_LOG_TRIVIAL(warning)
					<< "RISAConfiguration: Did you mean to use the processor stage '" << stageName
					<< "' in library '" << key.substr(0, key.size() - stageName.size() - 1) << "'?";
			}
		}
		for(const auto& [key, val] : sinkDict)
		{
			if(key.ends_with(stageName))
			{
				BOOST_LOG_TRIVIAL(warning)
					<< "RISAConfiguration: Did you mean to use the sink stage '" << stageName
					<< "' in library '" << key.substr(0, key.size() - stageName.size() - 1) << "'?";
			}
		}
	};

	for(auto libPath : uniqueLibraryPaths)
	{
		if(!std::filesystem::exists(libPath))
		{
			BOOST_LOG_TRIVIAL(fatal) << "Library file '" << libPath << "' does not exist.";
			return false;
		}

		void* libHandle = dlopen(libPath.c_str(), RTLD_NOW | RTLD_GLOBAL);

		if(libHandle == nullptr)
		{
			const char* dlsym_error = dlerror();

			if(dlsym_error != nullptr)
			{
				BOOST_LOG_TRIVIAL(error) << "RISAConfiguration: Error when trying to load library '"
										 << libPath << "': " << std::string(dlsym_error);
			}
			BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Library file " << libPath
									 << " could not be loaded";
			return false;
		}

		std::string libName = getStrFromLibraryFunction(libHandle, "get_library_name");

		if(libName.empty())
		{
			BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Unable to get library name for " << libPath
									 << ".";
			return false;
		}

		auto libVersion = getStrFromLibraryFunction(libHandle, "get_library_version");

		if(libVersion.empty())
		{
			BOOST_LOG_TRIVIAL(warning)
				<< "RISAConfiguration: Unable to get library version for " << libPath << ".";
			libVersion = ">NOT PROVIDED<";
		}
		BOOST_LOG_TRIVIAL(info) << "Loading library '" << libName << "', version '" << libVersion << "'";

#ifndef NDEBUG
		const std::string buildType = "DEBUG";
#else
		const std::string buildType = "RELEASE";
#endif
		if(!libVersion.starts_with(buildType))
		{
			BOOST_LOG_TRIVIAL(warning) << "Library '" << libName << "' is not a '" << buildType << "' build!";
		}

		std::vector<std::vector<std::string>> availableSources;
		std::vector<std::vector<std::string>> availableProcessors;
		std::vector<std::vector<std::string>> availableSinks;
		if(!(getStrArrayFromLibraryFunction(libHandle, "get_implemented_sources", availableSources)
			   && getStrArrayFromLibraryFunction(libHandle, "get_implemented_processors", availableProcessors)
			   && getStrArrayFromLibraryFunction(libHandle, "get_implemented_sinks", availableSinks)))
		{
			BOOST_LOG_TRIVIAL(fatal)
				<< "RISAConfiguration: Unable to get list of implemented stages types for " << libName << ".";
			return false;
		}

		if(!availableSources.empty() && !availableSources[0].empty())
		{
			for(const auto& source : availableSources)
			{
				sourceDict[libName + '_' + source[0]] =
					std::make_tuple(std::vector<std::string>{source[1]}, libHandle);
			}
		}
		printStagesInLibrary(libName, sourceDict, "source");

		if(!availableProcessors.empty() && !availableProcessors[0].empty())
		{
			for(const auto& proc : availableProcessors)
			{
				processorDict[libName + '_' + proc[0]] =
					std::make_tuple(std::vector<std::string>{proc[1], proc[2]}, libHandle);
			}
		}
		printStagesInLibrary(libName, processorDict, "process");

		if(!availableSinks.empty() && !availableSinks[0].empty())
		{
			for(const auto& sink : availableSinks)
			{
				sinkDict[libName + '_' + sink[0]] =
					std::make_tuple(std::vector<std::string>{sink[1]}, libHandle);
			}
		}
		printStagesInLibrary(libName, sinkDict, "sink");

		BOOST_LOG_TRIVIAL(debug) << "Storing libHandle at " << libHandle << " for " << libPath;
		m_libHandles.push_back(libHandle);
		m_libHandleNames.push_back(libPath);
	}

	// read pipeline configuration (stages, connections and observers)
	std::vector<StageDescriptor> stageDescriptors;
	auto stageConfig = cfg.lookup("pipeline.Stages");
	for(auto& grp : stageConfig)
	{
		StageDescriptor stage;
		stage.m_numInputs = grp["numInputs"].get<int>();
		stage.m_numOutputs = grp["numOutputs"].get<int>();
		stage.m_stageName = grp["name"].get<std::string>();
		stage.m_stageType = getStageTypeFromString(grp["type"].get<std::string>());
		stage.m_parameterSet = grp["parameterSet"].get<std::string>();
		stage.m_identifier = grp["identifier"].get<std::string>();

		try
		{
			stage.m_library = grp["library"].get<std::string>();
		}
		catch(...)
		{
			stage.m_library = "RISA_Core";
		}

		stageDescriptors.push_back(stage);
	}

	std::vector<ConnectionDescriptor> connectionDescriptors;
	auto connectionConfig = cfg.lookup("pipeline.Connections");
	for(auto& grp : connectionConfig)
	{
		ConnectionDescriptor connection;
		connection.m_fromStageIdentifier = grp["fromStage"].get<std::string>();
		connection.m_toStageIdentifier = grp["toStage"].get<std::string>();
		connection.m_toPort = grp["toPort"].get<int>();
		connectionDescriptors.push_back(connection);
	}

	std::string runtimeConfigPath;
	cfg.lookupValue("pipeline.runtimeConfigPath", runtimeConfigPath);
	FORMAT_PATH(runtimeConfigPath);
	std::vector<ObserverDescriptor> observerDescriptors;
	auto observerConfig = cfg.lookup("pipeline.Observers");
	for(auto& grp : observerConfig)
	{
		ObserverDescriptor observer;
		observer.m_stageIdentifier = grp["stage"].get<std::string>();
		observer.m_fileName = grp["file"].get<std::string>();
		observerDescriptors.push_back(observer);
	}

	// validate configuration
	// stages:
	// - must not have duplicate identifiers
	// - source: num input == 0
	// - process: num input > 0, num output > 0
	// - sink: num output == 0
	std::set<std::string> stageIdentifierStrings;
	for(auto& stage : stageDescriptors)
	{
		if(stageIdentifierStrings.contains(stage.m_identifier))
		{
			// identifier was used before
			BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Stage '" << stage.m_identifier
									 << "' was created more than once. Use unique stage identifiers.";
			return false;
		}
		stageIdentifierStrings.insert(stage.m_identifier);

		if(stage.m_stageType == detail::RISAStageType::Source && stage.m_numInputs > 0)
		{
			BOOST_LOG_TRIVIAL(warning)
				<< "RISAConfiguration: Stage '" << stage.m_identifier << "' is of type 'source' but has "
				<< stage.m_numInputs << " inputs. Number of inputs was set to 0.";
			stage.m_numInputs = 0;
		}
		if(stage.m_stageType == detail::RISAStageType::Sink && stage.m_numOutputs > 0)
		{
			BOOST_LOG_TRIVIAL(warning)
				<< "RISAConfiguration: Stage '" << stage.m_identifier << "' is of type 'sink' but has "
				<< stage.m_numInputs << " outputs. Number of outputs was set to 0.";
			stage.m_numOutputs = 0;
		}
		if(stage.m_stageType == detail::RISAStageType::Process && stage.m_numInputs <= 0)
		{
			BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Stage '" << stage.m_identifier
									 << "' is of type 'process' but has " << stage.m_numInputs
									 << " inputs. Must be > 0.";
			return false;
		}
		if(stage.m_stageType == detail::RISAStageType::Process && stage.m_numOutputs <= 0)
		{
			BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Stage '" << stage.m_identifier
									 << "' is of type 'process' but has " << stage.m_numOutputs
									 << " outputs. Must be > 0.";
			return false;
		}
	}

	// connections:
	// - stage names must be created before
	// - each stage must be connected to as many stages as defined in respective numInputs / numOutputs
	// - toStage/toPort combintions must be unique
	// - port num must be < numInputs
	std::map<std::string, std::tuple<int, int, int>>
		stageConnections; // tuple: in_total, in_remaining, out_remaining
	std::set<std::tuple<std::string, int>> toStagePorts;
	for(const auto& stage : stageDescriptors)
	{
		stageConnections[stage.m_identifier] =
			std::make_tuple(stage.m_numInputs, stage.m_numInputs, stage.m_numOutputs);
	}
	for(auto connection : connectionDescriptors)
	{
		if(!stageIdentifierStrings.contains(connection.m_fromStageIdentifier))
		{
			BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Stage '" << connection.m_fromStageIdentifier
									 << "' was not created before requesting to connect it.";
			return false;
		}
		if(!stageIdentifierStrings.contains(connection.m_toStageIdentifier))
		{
			BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Stage '" << connection.m_toStageIdentifier
									 << "' was not created before requesting to connect it.";
			return false;
		}
		if(std::get<0>(stageConnections[connection.m_toStageIdentifier]) <= connection.m_toPort)
		{
			BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Cannot connect '"
									 << connection.m_fromStageIdentifier << "' to '"
									 << connection.m_toStageIdentifier << " at port " << connection.m_toPort
									 << ". Not enough inputs.";
			return false;
		}
		if(std::get<1>(stageConnections[connection.m_toStageIdentifier]) == 0)
		{
			BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Cannot connect '"
									 << connection.m_fromStageIdentifier << "' to '"
									 << connection.m_toStageIdentifier << " at port " << connection.m_toPort
									 << ". Not enough inputs.";
			return false;
		}
		if(std::get<2>(stageConnections[connection.m_fromStageIdentifier]) == 0)
		{
			BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Cannot connect '"
									 << connection.m_fromStageIdentifier << "' to '"
									 << connection.m_toStageIdentifier << "'. Not enough outputs.";
			return false;
		}
		auto toStagePort = std::make_tuple(connection.m_toStageIdentifier, connection.m_toPort);
		if(toStagePorts.contains(toStagePort))
		{
			BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Requested to connect to stage '"
									 << connection.m_toStageIdentifier << "' at port " << connection.m_toPort
									 << " multiple times.";
			return false;
		}

		toStagePorts.insert(toStagePort);
		std::get<1>(stageConnections[connection.m_toStageIdentifier]) -= 1;
		std::get<2>(stageConnections[connection.m_fromStageIdentifier]) -= 1;
	}
	for(auto stageConnection : stageConnections)
	{
		if(std::get<1>(stageConnection.second) > 0)
		{
			// a value > 0 means we did not use all available inputs
			BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Not all inputs of '" << stageConnection.first
									 << "' were connected.";
			return false;
		}
		if(std::get<2>(stageConnection.second) > 0)
		{
			// a value > 0 means we did not use all available inputs
			BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Not all outputs of '" << stageConnection.first
									 << "' were connected.";
			return false;
		}
	}

	// observers:
	//  - stage to attach to must exist
	for(const auto& observer : observerDescriptors)
	{
		if(!stageIdentifierStrings.contains(observer.m_stageIdentifier))
		{
			BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Cannot connect observer to '"
									 << observer.m_stageIdentifier << "'. Stage was not defined.";
			return false;
		}
	}

	std::map<std::string, std::shared_ptr<glados::pipeline::GenericStage>> stagesMap;
	for(const auto& stage : stageDescriptors)
	{
		// create all stages

		// get stage info from config file
		auto stageName = stage.m_library + '_' + stage.m_stageName;
		// create stage
		try
		{

			if(stage.m_stageType == detail::RISAStageType::Source)
			{
				if(sourceDict.contains(stageName))
				{
					try
					{
						if(!RISAModuleInterfaceHelpers::addSourceStage(*m_pipeline, stagesMap,
							   std::get<1>(sourceDict[stageName]), stage.m_identifier, stage.m_stageName,
							   std::get<0>(sourceDict[stageName])[0], configFile, stage.m_parameterSet))
						{
							BOOST_LOG_TRIVIAL(fatal)
								<< "RISAConfiguration: Stage '" << stage.m_identifier << "' of type '"
								<< stage.m_stageName << "' could not be added successfully.";
							return false;
						}
					}
					catch(std::runtime_error const& err)
					{
						if(std::strcmp(err.what(), "Unable to load library function.") == 0)
						{
							BOOST_LOG_TRIVIAL(fatal)
								<< "RISAConfiguration: Source stage '" << stage.m_identifier << "' of type '"
								<< stage.m_stageName << "' is not provided correctly in library "
								<< stage.m_library << ".";
							BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Refer to the provider of "
													 << stage.m_library << " to get a corrected version.";
						}
						else
						{
							BOOST_LOG_TRIVIAL(fatal)
								<< "RISAConfiguration: Error initializing stage '" << stage.m_identifier
								<< "' of type '" << stage.m_stageName << "': " << err.what();
						}
						return false;
					}
				}
				else
				{
					BOOST_LOG_TRIVIAL(fatal)
						<< "RISAConfiguration: Source stage '" << stage.m_identifier << "' of type '"
						<< stage.m_stageName << "' not available in library '" << stage.m_library << "'.";
					printStagesInLibrary(stage.m_library, sourceDict, "source", "warning");
					printStageHint(stage.m_stageName);
					return false;
				}
			}
			else if(stage.m_stageType == detail::RISAStageType::Process)
			{
				if(processorDict.contains(stageName))
				{
					try
					{
						if(!RISAModuleInterfaceHelpers::addProcessorStage(*m_pipeline, stagesMap,
							   std::get<1>(processorDict[stageName]), stage.m_identifier, stage.m_stageName,
							   std::get<0>(processorDict[stageName])[0],
							   std::get<0>(processorDict[stageName])[1], configFile, stage.m_parameterSet,
							   stage.m_numInputs, stage.m_numOutputs))
						{
							BOOST_LOG_TRIVIAL(fatal)
								<< "RISAConfiguration: Stage '" << stage.m_identifier << "' of type '"
								<< stage.m_stageName << "' could not be added successfully.";
							return false;
						}
					}
					catch(std::runtime_error const& err)
					{
						if(std::strcmp(err.what(), "Unable to load library function.") == 0)
						{
							BOOST_LOG_TRIVIAL(fatal)
								<< "RISAConfiguration: Processor stage '" << stage.m_identifier
								<< "' of type '" << stage.m_stageName
								<< "' is not provided correctly in library " << stage.m_library << ".";
							BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Refer to the provider of "
													 << stage.m_library << " to get a corrected version.";
						}
						else
						{
							BOOST_LOG_TRIVIAL(fatal)
								<< "RISAConfiguration: Error initializing stage '" << stage.m_identifier
								<< "' of type '" << stage.m_stageName << "': " << err.what();
						}
						return false;
					}
				}
				else
				{
					BOOST_LOG_TRIVIAL(fatal)
						<< "RISAConfiguration: Processor stage '" << stage.m_identifier << "' of type '"
						<< stage.m_stageName << "' not available in library '" << stage.m_library << "'.";
					printStagesInLibrary(stage.m_library, processorDict, "process", "warning");
					printStageHint(stage.m_stageName);
					return false;
				}
			}
			else if(stage.m_stageType == detail::RISAStageType::Sink)
			{
				if(sinkDict.contains(stageName))
				{
					try
					{
						if(!RISAModuleInterfaceHelpers::addSinkStage(*m_pipeline, stagesMap,
							   std::get<1>(sinkDict[stageName]), stage.m_identifier, stage.m_stageName,
							   std::get<0>(sinkDict[stageName])[0], configFile, stage.m_parameterSet))
						{
							BOOST_LOG_TRIVIAL(fatal)
								<< "RISAConfiguration: Stage '" << stage.m_identifier << "' of type '"
								<< stage.m_stageName << "' could not be added successfully.";
							return false;
						}
					}
					catch(std::runtime_error const& err)
					{
						if(std::strcmp(err.what(), "Unable to load library function.") == 0)
						{
							BOOST_LOG_TRIVIAL(fatal)
								<< "RISAConfiguration: Sink stage '" << stage.m_identifier << "' of type '"
								<< stage.m_stageName << "' is not provided correctly in library "
								<< stage.m_library << ".";
							BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Refer to the provider of "
													 << stage.m_library << " to get a corrected version.";
						}
						else
						{
							BOOST_LOG_TRIVIAL(fatal)
								<< "RISAConfiguration: Error initializing stage '" << stage.m_identifier
								<< "' of type '" << stage.m_stageName << "': " << err.what();
						}
						return false;
					}
				}
				else
				{
					BOOST_LOG_TRIVIAL(fatal)
						<< "RISAConfiguration: Sink stage '" << stage.m_identifier << "' of type '"
						<< stage.m_stageName << "' not available in library '" << stage.m_library << "'.";
					printStagesInLibrary(stage.m_library, sinkDict, "sink", "warning");
					printStageHint(stage.m_stageName);
					return false;
				}
			}
		}
		catch(std::runtime_error const& err)
		{
			// throw up exceptions to be handled by caller
			BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Exception during initialization of stage '"
									 << stage.m_identifier << "' with type '" << stage.m_stageName
									 << "': " << err.what();
			return false;
		}
	}

	// connect stages
	for(const auto& connection : connectionDescriptors)
	{
		try
		{
			m_pipeline->connectSpecificInput(stagesMap.at(connection.m_fromStageIdentifier),
				stagesMap.at(connection.m_toStageIdentifier), connection.m_toPort);
		}
		catch(std::out_of_range&)
		{
			BOOST_LOG_TRIVIAL(fatal) << "Cannot connect '" << connection.m_fromStageIdentifier << "' to '"
									 << connection.m_toStageIdentifier
									 << "' - One of the stages was not defined.";
			return false;
		}
		catch(std::runtime_error& err)
		{
			BOOST_LOG_TRIVIAL(fatal) << "Cannot connect '" << connection.m_fromStageIdentifier << "' to '"
									 << connection.m_toStageIdentifier << "': " << err.what();
			return false;
		}
	}

	// create observers
	for(const auto& observer : observerDescriptors)
	{
		try
		{
			m_subjects.emplace_back(std::make_shared<glados::FileObserver>());

			dynamic_cast<glados::FileObserver*>(m_subjects.back().get())
				->init(stagesMap[observer.m_stageIdentifier].get(), runtimeConfigPath, observer.m_fileName,
					IN_CLOSE_WRITE);
			if(!(dynamic_cast<glados::FileObserver*>(m_subjects.back().get())->m_initOk))
			{
				BOOST_LOG_TRIVIAL(fatal)
					<< "Unable to create Observer for " << observer.m_stageIdentifier << " ("
					<< runtimeConfigPath << observer.m_fileName << "): Could not initialize file paths.";
				return false;
			}
		}
		catch(std::runtime_error const& err)
		{
			BOOST_LOG_TRIVIAL(fatal) << "Unable to create Observer for " << observer.m_stageIdentifier << " ("
									 << runtimeConfigPath << observer.m_fileName << "): " << err.what();
			return false;
		}
	}

	// create default observer for signal handling
	std::string tempPath = setupTempPath();
	for(auto& stage : stageDescriptors)
	{
		if(stage.m_stageType == detail::RISAStageType::Source)
		{
			try
			{
				m_subjects.emplace_back(std::make_shared<glados::FileObserver>());
#ifdef __linux__
				dynamic_cast<glados::FileObserver*>(m_subjects.back().get())
					->init(
						stagesMap[stage.m_identifier].get(), tempPath, "signalReceived.cfg", IN_CLOSE_WRITE);
#elif _WIN32
				dynamic_cast<glados::FileObserver*>(m_subjects.back().get())
					->init(
						stagesMap[stage.m_identifier].get(), tempPath, "signalReceived.cfg", IN_CLOSE_WRITE);
#endif
				if(!(dynamic_cast<glados::FileObserver*>(m_subjects.back().get())->m_initOk))
				{
					BOOST_LOG_TRIVIAL(fatal)
						<< "Unable to create default Observer for temporary path: " << tempPath <<"signalReceived.cfg";
					return false;
				}
			}
			catch(std::runtime_error const& err)
			{
				BOOST_LOG_TRIVIAL(fatal) << "Unable to create Observer for " << stage.m_identifier << " ("
										 << tempPath << "signalReceived.cfg): " << err.what();
				return false;
			}
		}
	}

	m_isConfigured = true;
	return true;
}

auto setupTempPath() -> std::string
{
#ifdef _WIN32
#include <windows.h>
	TCHAR winTempPath[MAX_PATH + 1];
	DWORD dwRetVal = GetTempPath(sizeof(winTempPath), winTempPath);
	if((dwRetVal < MAX_PATH) && (dwRetVal != 0))
	{
		if(winTempPath[lstrlen(winTempPath) - 1] != '\\')
		{
			lstrcat(winTempPath, "\\");
		}
		std::string tmp(winTempPath);
		FORMAT_PATH(tmp);
		return tmp;
	}
	BOOST_LOG_TRIVIAL(warning) << "Unable to determine temporary path. Fallback to 'C:\\\\temp\\'";
	return std::string("C:\\\\temp\\");
#elif __linux__
	return std::string{"/tmp/"};
#endif
}

auto RISAConfiguration::startConfiguration() -> void
{
	if(!m_isConfigured)
	{
		BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration:: Configuration was not configured before starting.";
		return;
	}
	try
	{
		startSubjects();
		BOOST_LOG_TRIVIAL(debug) << "RISAConfiguration:: Subjects started.";
		m_pipeline->runAll();
		BOOST_LOG_TRIVIAL(debug) << "RISAConfiguration:: Pipeline started.";
		m_isRunning = true;
	}
	catch(...)
	{
		throw;
	}
}

auto RISAConfiguration::waitUntilFinished() -> void
{
	if(!m_isRunning)
	{
		BOOST_LOG_TRIVIAL(error) << "RISAConfiguration:: Requested to stop non-running configuration.";
		return;
	}
	m_pipeline->wait();
	BOOST_LOG_TRIVIAL(debug) << "RISAConfiguration:: Pipeline terminated.";
	stopSubjects();
	BOOST_LOG_TRIVIAL(debug) << "RISAConfiguration:: Subjects terminated.";
	m_isRunning = false;
}

auto RISAConfiguration::startSubjects() -> void
{
	for(const auto& subject : m_subjects)
	{
		auto* fileObserver = dynamic_cast<glados::FileObserver*>(subject.get());
		if(fileObserver->m_initOk)
		{
			fileObserver->start();
		}
		else
		{
			BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration:: Cannot start FileObserver with path "
									 << fileObserver->m_filePath << fileObserver->m_fileName << ".";
			throw std::runtime_error("Unable to start FileOberver");
		}
	}
}

auto RISAConfiguration::stopSubjects() -> void
{
	for(const auto& subject : m_subjects)
	{
		auto* fileObserver = dynamic_cast<glados::FileObserver*>(subject.get());
		if(fileObserver->m_initOk)
		{
			fileObserver->wait();
		}
	}
}
}
