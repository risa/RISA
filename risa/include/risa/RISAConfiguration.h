// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef RISA_CONFIGURATION_H_
#define RISA_CONFIGURATION_H_

#include "RISAModuleInterface.h"
#include "RISAModuleInterfaceHelpers.h"

#include "ConfigReader.h"

#include <glados/Image.h>
#include <glados/ImageLoader.h>
#include <glados/ImageSaver.h>
#include <glados/observer/FileObserver.h>

#include <glados/pipeline/Pipeline.h>
#include <glados/pipeline/SinkStage.h>
#include <glados/pipeline/SourceStage.h>
#include <glados/pipeline/Stage.h>

#include <glados/cuda/HostMemoryManager.h>

#include <glados/observer/Subject.h>
#include <glados/pipeline/Pipeline.h>

#include <map>
#include <set>
#include <string>

namespace risa
{

namespace detail
{

//! Available types of stages. Source stages generate data, Process stages process data, Sink stages remove
//! data from the pipeline.
enum class RISAStageType
{
	Source,
	Process,
	Sink
};
}

//! Determine system-wide temporary path
std::string setupTempPath();

//! Wrapper class for dynamic pipeline configuration based of a configuration file.
class RISAConfiguration
{
	public:
	RISAConfiguration(){};
	~RISAConfiguration();
	//! Generate a RISAConfiguration from the given configuration file.
	//! @param[in] configFile path to the configuration file
	//! @return true on success
	//! @return false on error
	bool generateConfiguration(std::string& configFile);
	//! Start the configured configuration
	void startConfiguration();
	//! Wait until the configured configuration finishes
	void waitUntilFinished();
	//! Deconstruct pipeline before closing dl handles
	void cleanupPipeline();

	protected:
	RISAConfiguration(const RISAConfiguration&) = delete;
	RISAConfiguration(RISAConfiguration&&) = default;

	private:
	bool m_isConfigured{false}; //! whether pipeline was configured successfully
	bool m_isRunning{false}; //! whether pipeline is currently running
	std::unique_ptr<glados::pipeline::Pipeline> m_pipeline; //! the glados pipeline object
	std::vector<std::shared_ptr<glados::Subject>>
		m_subjects; //! list of subjects which have observers (stages) attached
	//! Start all the defined subjects
	void startSubjects();
	//! Trigger and wait for termination of all defined subjects
	void stopSubjects();

	//! Struct describing all attributes of a risa stage as configured in the config file
	struct StageDescriptor
	{
		int m_numInputs; //! number of inputs into the stage. Must be 0 for Source stages.
		int m_numOutputs; //! number of outputs from the stage. Must be 0 for Sink stages.
		detail::RISAStageType m_stageType; //! the stage type (Source, Process, or Sink)
		std::string m_stageName; //! name of the stage implementation
		std::string m_parameterSet; //! path in the config file to the respective configuration for this stage
		std::string m_identifier; //! unique identifier of the stage
		std::string m_library{"RISACore"}; //! the library which implements this stage of name stageName
	};

	//! Struct describing a connections between two stages
	struct ConnectionDescriptor
	{
		std::string m_fromStageIdentifier,
			m_toStageIdentifier; //! identifier or the producer (fromStageIdentifier) and consumer
								//! (toStageIdentifier)
		int m_toPort; //! the port to connect to on the consumer stage
	};

	//! Struct describing the observer (currently only FileObserver)
	struct ObserverDescriptor
	{
		std::string m_stageIdentifier; //! identifier of the stage
		std::string m_fileName; //! file name to observe
	};

	detail::RISAStageType getStageTypeFromString(const std::string& type)
	{
		if(type == "Sink")
			return detail::RISAStageType::Sink;
		if(type == "Process")
			return detail::RISAStageType::Process;
		if(type == "Source")
			return detail::RISAStageType::Source;

		throw std::runtime_error("Unknown stage type");
	}

	//! vector of currently open library handles
	std::vector<void*> m_libHandles;
	std::vector<std::string> m_libHandleNames;
};

}

#endif // !RISA_CONFIGURATION_H_
