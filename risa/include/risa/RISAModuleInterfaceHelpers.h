// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors: 
// Dominic Windisch (d.windisch@hzdr.de), 
// André Bieberle (a.bieberle@hzdr.de)

#ifndef RISA_MODULE_INTERFACE_HELPERS
#define RISA_MODULE_INTERFACE_HELPERS

#pragma once

#include "RISAModuleInterface.h"

#include <glados/pipeline/Pipeline.h>
#include <glados/pipeline/SinkStage.h>
#include <glados/pipeline/SourceStage.h>
#include <glados/pipeline/Stage.h>

#ifdef __linux__
#include <dlfcn.h> // dlopen(), dlsym()
#elif _WIN32
#include "../dlfcn-win32/dlfcn.h"
#endif

#include <map>
#include <string>
#include <vector>

// HELPER CLASSES
template <class implementation_t, typename implementation_ctor_t, typename implementation_dtor_t>
class DynamicallyLoadedProcessorStage
{
	public:
	DynamicallyLoadedProcessorStage(implementation_ctor_t* ctor, implementation_dtor_t* dtor,
		const std::string& configFile, const std::string parameterSet, const int numberOfOutputs,
		const int numberOfInputs)
		: implementation_dtor{dtor}
	{
		implementation = ctor(configFile.data(), parameterSet.data(), numberOfOutputs, numberOfInputs);
	}

	~DynamicallyLoadedProcessorStage() { implementation_dtor(implementation); }

	using input_type = typename implementation_t::input_type;
	using output_type = typename implementation_t::output_type;

	void update(glados::Subject* s) { implementation->update(s); }
	void process(input_type&& img, int input_idx)
	{
		implementation->process(std::forward<input_type>(img), input_idx);
	}
	output_type wait(int output_idx) { return implementation->wait(output_idx); }

	private:
	implementation_t* implementation;
	implementation_dtor_t* implementation_dtor;
};

template <class implementation_t, typename implementation_ctor_t, typename implementation_dtor_t>
class DynamicallyLoadedSourceStage
{
	public:
	DynamicallyLoadedSourceStage(implementation_ctor_t* ctor, implementation_dtor_t* dtor,
		const std::string& configFile, const std::string parameterSet)
		: implementation_dtor{dtor}
	{
		implementation = ctor(configFile.data(), parameterSet.data());
	}

	~DynamicallyLoadedSourceStage() { implementation_dtor(implementation); }

	using output_type = typename implementation_t::output_type;

	void update(glados::Subject* s) { implementation->update(s); }
	output_type loadImage() { return implementation->loadImage(); }

	private:
	implementation_t* implementation;
	implementation_dtor_t* implementation_dtor;
};

template <class implementation_t, typename implementation_ctor_t, typename implementation_dtor_t>
class DynamicallyLoadedSinkStage
{
	public:
	DynamicallyLoadedSinkStage(implementation_ctor_t* ctor, implementation_dtor_t* dtor,
		const std::string& configFile, const std::string parameterSet)
		: implementation_dtor{dtor}
	{
		implementation = ctor(configFile.data(), parameterSet.data());
	}

	~DynamicallyLoadedSinkStage() { implementation_dtor(implementation); }

	using input_type = typename implementation_t::input_type;

	void update(glados::Subject* s) { implementation->update(s); }
	void saveImage(input_type&& img) { return implementation->saveImage(std::forward<input_type>(img)); }

	private:
	implementation_t* implementation;
	implementation_dtor_t* implementation_dtor;
};

// STAGE ADDERS

template <typename fctn_signature>
fctn_signature* dlsym_s(void* libHandle, const std::string fctnName)
{
	dlerror();
	fctn_signature* get_fctn = (fctn_signature*)dlsym(libHandle, fctnName.c_str());
	const char* dlsym_error = dlerror();
	if(dlsym_error)
	{
		BOOST_LOG_TRIVIAL(fatal) << "RISAModuleInterface: Could not load function '" << fctnName << "'.";
		throw std::runtime_error("Unable to load library function.");
	}

	return get_fctn;
}
namespace RISAModuleInterfaceHelpers
{

inline bool addSourceStage(glados::pipeline::Pipeline& pipeline,
	std::map<std::string, std::shared_ptr<glados::pipeline::GenericStage>>& stagesMap, void* libHandle,
	const std::string identifier, const std::string name, const std::string type_out,
	const std::string cfg_file, const std::string params)
{
	// clang-format off
	if(type_out == "cfloat")
	{
		using dynStageType = RISAModule_Source<cuda_array<float>>;
		using dynStageCtor = RISAModule_Source<cuda_array<float>>*(const char* cf, const char* ps);
		using dynStageDtor = void(RISAModule_Source<cuda_array<float>>*);
		dynStageDtor* destroy_stage = dlsym_s<dynStageDtor>(libHandle, "destroy_" + name);
		dynStageCtor* create_stage = dlsym_s<dynStageCtor>(libHandle, "create_" + name);
		stagesMap[identifier] = std::move(
			pipeline.create<glados::pipeline::SourceStage<DynamicallyLoadedSourceStage<dynStageType, dynStageCtor, dynStageDtor>>>(
				cfg_file, params, create_stage, destroy_stage));
	}
	else if(type_out == "huint16_t")
	{
        using dynStageType = RISAModule_Source<host_array<uint16_t>>;
        using dynStageCtor = RISAModule_Source<host_array<uint16_t>>*(const char* cf, const char* ps);
        using dynStageDtor = void(RISAModule_Source<host_array<uint16_t>>*);
        dynStageDtor* destroy_stage = dlsym_s<dynStageDtor>(libHandle, ("destroy_" + name).c_str());
        dynStageCtor* create_stage = dlsym_s<dynStageCtor>(libHandle, ("create_" + name).c_str());
        stagesMap[identifier] = std::move(
                pipeline.create<
                        glados::pipeline::SourceStage<DynamicallyLoadedSourceStage<dynStageType, dynStageCtor,dynStageDtor>>>(
                        cfg_file, params, create_stage, destroy_stage));
	}
	else if(type_out == "cuint16_t")
	{
        using dynStageType = RISAModule_Source<cuda_array<uint16_t>>;
        using dynStageCtor = RISAModule_Source<cuda_array<uint16_t>>*(const char* cf, const char* ps);
        using dynStageDtor = void(RISAModule_Source<cuda_array<uint16_t>>*);
        dynStageDtor* destroy_stage = dlsym_s<dynStageDtor>(libHandle, ("destroy_" + name).c_str());
        dynStageCtor* create_stage = dlsym_s<dynStageCtor>(libHandle, ("create_" + name).c_str());
        stagesMap[identifier] = std::move(
                pipeline.create<
                        glados::pipeline::SourceStage<DynamicallyLoadedSourceStage<dynStageType, dynStageCtor,dynStageDtor>>>(
                        cfg_file, params, create_stage, destroy_stage));
	}	
	else
	{
		const int addCodeInThisLine = __LINE__ - 2;
		if (type_out[0] != 'c' && type_out[0] != 'h') {
			BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Requested output type '"<<type_out<<"'is not supported. Types must be start with 'c' to indicate cuda_arrays or 'h' to indicate host_arrays.";
			return false;
		}
		std::string dev_out = (type_out[0] == 'c' ? "cuda" : "host");
		std::string basetype_out = type_out.substr(1);
		const std::string RISAModuleType = dev_out+"_array<"+basetype_out+">";
		
		std::string code = "\n"
						   "else if(type_out == \"" + type_out + "\")\n"
			  "{\n"
			  "	using dynStageType = RISAModule_Source<"+RISAModuleType+">;\n"
			  "	using dynStageCtor = RISAModule_Source<"+RISAModuleType+">*(const char* cf, const char* ps);\n"
			  "	using dynStageDtor = void(RISAModule_Source<"+RISAModuleType+">*);\n"
			  "	dynStageDtor* destroy_stage = dlsym_s<dynStageDtor>(libHandle, (\"destroy_\" + name).c_str());\n"
			  "	dynStageCtor* create_stage = dlsym_s<dynStageCtor>(libHandle, (\"create_\" + name).c_str());\n"
			  "	stagesMap[identifier] = std::move(\n"
			  "		pipeline.create<\n"
			  "			glados::pipeline::SourceStage<DynamicallyLoadedSourceStage<dynStageType, dynStageCtor,dynStageDtor>>>(\n"
			  "			cfg_file, params, create_stage, destroy_stage));\n"
			  "}\n\n";

		BOOST_LOG_TRIVIAL(fatal)
			<< "RISAConfiguration: Requested output type '"<<type_out<<"' is currently not implemented.";
		BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Add the following code snippet to \n\n"
								 << __FILE__ << " at line " << addCodeInThisLine << ":\n"
								 << code;
		return false;
	}
	return true;
	// clang-format on
}

inline bool addProcessorStage(glados::pipeline::Pipeline& pipeline,
	std::map<std::string, std::shared_ptr<glados::pipeline::GenericStage>>& stagesMap, void* libHandle,
	const std::string identifier, const std::string name, const std::string type_in,
	const std::string type_out, const std::string cfg_file, const std::string params, const int numIn,
	const int numOut)
{
	const std::string combinedType = type_in + "_" + type_out;

	// clang-format off
	if(combinedType == "cfloat_cfloat")
	{
		using dynStageType = RISAModule_Processor<cuda_array<float>, cuda_array<float>>;
		using dynStageCtor = RISAModule_Processor<cuda_array<float>, cuda_array<float>>*(const char* cf, const char* ps, const int no, const int ni);
		using dynStageDtor = void(RISAModule_Processor<cuda_array<float>, cuda_array<float>>*);
		dynStageDtor* destroy_stage = dlsym_s<dynStageDtor>(libHandle, ("destroy_" + name).c_str());
		dynStageCtor* create_stage = dlsym_s<dynStageCtor>(libHandle, ("create_" + name).c_str());
		stagesMap[identifier] = std::move(
			pipeline.create<glados::pipeline::Stage<DynamicallyLoadedProcessorStage<dynStageType, dynStageCtor, dynStageDtor>>>(
				numIn, numOut, create_stage, destroy_stage, cfg_file, params));
	}
	else if(combinedType == "huint16_t_cuint16_t")
	{
		using dynStageType = RISAModule_Processor<host_array<uint16_t>, cuda_array<uint16_t>>;
		using dynStageCtor = RISAModule_Processor<host_array<uint16_t>, cuda_array<uint16_t>>*(const char* cf, const char* ps, const int no, const int ni);
		using dynStageDtor = void(RISAModule_Processor<host_array<uint16_t>, cuda_array<uint16_t>>*);
		dynStageDtor* destroy_stage = dlsym_s<dynStageDtor>(libHandle, ("destroy_" + name).c_str());
		dynStageCtor* create_stage = dlsym_s<dynStageCtor>(libHandle, ("create_" + name).c_str());
		stagesMap[identifier] = std::move(
			pipeline.create<glados::pipeline::Stage<DynamicallyLoadedProcessorStage<dynStageType, dynStageCtor, dynStageDtor>>>(
				numIn, numOut, create_stage, destroy_stage, cfg_file, params));
	}
	else if(combinedType == "cuint16_t_cfloat")
	{
        using dynStageType = RISAModule_Processor<cuda_array<uint16_t>, cuda_array<float>>;
        using dynStageCtor = RISAModule_Processor<cuda_array<uint16_t>, cuda_array<float>>*(const char* cf, const char* ps, const int no, const int ni);
        using dynStageDtor = void(RISAModule_Processor<cuda_array<uint16_t>, cuda_array<float>>*);
        dynStageDtor* destroy_stage = dlsym_s<dynStageDtor>(libHandle, ("destroy_" + name).c_str());
        dynStageCtor* create_stage = dlsym_s<dynStageCtor>(libHandle, ("create_" + name).c_str());
        stagesMap[identifier] = std::move(
                pipeline.create<
                        glados::pipeline::Stage<DynamicallyLoadedProcessorStage<dynStageType, dynStageCtor,dynStageDtor>>>(
                        numIn, numOut, create_stage, destroy_stage, cfg_file, params));
	}
	else if(combinedType == "cfloat_hfloat")
	{
        using dynStageType = RISAModule_Processor<cuda_array<float>, host_array<float>>;
        using dynStageCtor = RISAModule_Processor<cuda_array<float>, host_array<float>>*(const char* cf, const char* ps, const int no, const int ni);
        using dynStageDtor = void(RISAModule_Processor<cuda_array<float>, host_array<float>>*);
        dynStageDtor* destroy_stage = dlsym_s<dynStageDtor>(libHandle, ("destroy_" + name).c_str());
        dynStageCtor* create_stage = dlsym_s<dynStageCtor>(libHandle, ("create_" + name).c_str());
        stagesMap[identifier] = std::move(
                pipeline.create<
                        glados::pipeline::Stage<DynamicallyLoadedProcessorStage<dynStageType, dynStageCtor,dynStageDtor>>>(
                        numIn, numOut, create_stage, destroy_stage, cfg_file, params));
	}
	else
	{
		const int addCodeInThisLine = __LINE__ - 2;
		if (type_out[0] != 'c' && type_out[0] != 'h') {
			BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Requested output type '"<<type_out<<"'is not supported. Types must be start with 'c' to indicate cuda_arrays or 'h' to indicate host_arrays.";
			return false;
		}
		if (type_in[0] != 'c' && type_in[0] != 'h') {
			BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Requested input type '"<<type_in<<"'is not supported. Types must be start with 'c' to indicate cuda_arrays or 'h' to indicate host_arrays.";
			return false;
		}
		std::string dev_in = (type_in[0] == 'c' ? "cuda" : "host");
		std::string dev_out = (type_out[0] == 'c' ? "cuda" : "host");
		std::string basetype_in = type_in.substr(1);
		std::string basetype_out = type_out.substr(1);
		const std::string RISAModuleType = dev_in+"_array<"+basetype_in+">, "+dev_out+"_array<"+basetype_out+">";
		
		std::string code = "\n"
						   "else if(combinedType == \"" + combinedType + "\")\n"
			  "{\n"
			  "	using dynStageType = RISAModule_Processor<"+RISAModuleType+">;\n"
			  "	using dynStageCtor = RISAModule_Processor<"+RISAModuleType+">*(const char* cf, const char* ps, const int no, const int ni);\n"
			  "	using dynStageDtor = void(RISAModule_Processor<"+RISAModuleType+">*);\n"
			  "	dynStageDtor* destroy_stage = dlsym_s<dynStageDtor>(libHandle, (\"destroy_\" + name).c_str());\n"
			  "	dynStageCtor* create_stage = dlsym_s<dynStageCtor>(libHandle, (\"create_\" + name).c_str());\n"
			  "	stagesMap[identifier] = std::move(\n"
			  "		pipeline.create<\n"
			  "			glados::pipeline::Stage<DynamicallyLoadedProcessorStage<dynStageType, dynStageCtor,dynStageDtor>>>(\n"
			  "			numIn, numOut, create_stage, destroy_stage, cfg_file, params));\n"
			  "}\n\n";

		BOOST_LOG_TRIVIAL(fatal)
			<< "RISAConfiguration: Requested input/output type combination '"<<combinedType<<"' is currently not implemented.";
		BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Add the following code snippet to \n\n"
								 << __FILE__ << " at line " << addCodeInThisLine << ":\n"
								 << code;
		return false;
	}
	// clang-format on
	return true;
}

inline bool addSinkStage(glados::pipeline::Pipeline& pipeline,
	std::map<std::string, std::shared_ptr<glados::pipeline::GenericStage>>& stagesMap, void* libHandle,
	const std::string identifier, const std::string name, const std::string type_in,
	const std::string cfg_file, const std::string params)
{
	// clang-format off
	if(type_in == "cfloat")
	{
		using dynStageType = RISAModule_Sink<cuda_array<float>>;
		using dynStageCtor = RISAModule_Sink<cuda_array<float>>*(const char* cf, const char* ps);
		using dynStageDtor = void(RISAModule_Sink<cuda_array<float>>*);
		dynStageDtor* destroy_stage = dlsym_s<dynStageDtor>(libHandle, ("destroy_" + name).c_str());
		dynStageCtor* create_stage = dlsym_s<dynStageCtor>(libHandle, ("create_" + name).c_str());
		stagesMap[identifier] = std::move(
			pipeline.create<glados::pipeline::SinkStage<DynamicallyLoadedSinkStage<dynStageType, dynStageCtor, dynStageDtor>>>(
				cfg_file, params, create_stage, destroy_stage));
	}
	else if(type_in == "hfloat")
	{
        using dynStageType = RISAModule_Sink<host_array<float>>;
        using dynStageCtor = RISAModule_Sink<host_array<float>>*(const char* cf, const char* ps);
        using dynStageDtor = void(RISAModule_Sink<host_array<float>>*);
        dynStageDtor* destroy_stage = dlsym_s<dynStageDtor>(libHandle, ("destroy_" + name).c_str());
        dynStageCtor* create_stage = dlsym_s<dynStageCtor>(libHandle, ("create_" + name).c_str());
        stagesMap[identifier] = std::move(
                pipeline.create<
                        glados::pipeline::SinkStage<DynamicallyLoadedSinkStage<dynStageType, dynStageCtor,dynStageDtor>>>(
                        cfg_file, params, create_stage, destroy_stage));
	}
	else
	{
		const int addCodeInThisLine = __LINE__ - 2;
		if (type_in[0] != 'c' && type_in[0] != 'h') {
			BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Requested input type '"<<type_in<<"'is not supported. Types must be start with 'c' to indicate cuda_arrays or 'h' to indicate host_arrays.";
			return false;
		}
		std::string dev_in = (type_in[0] == 'c' ? "cuda" : "host");
		std::string basetype_in = type_in.substr(1);
		const std::string RISAModuleType = dev_in+"_array<"+basetype_in+">";
		
		std::string code = "\n"
						   "else if(type_in == \"" + type_in + "\")\n"
			  "{\n"
			  "	using dynStageType = RISAModule_Sink<"+RISAModuleType+">;\n"
			  "	using dynStageCtor = RISAModule_Sink<"+RISAModuleType+">*(const char* cf, const char* ps);\n"
			  "	using dynStageDtor = void(RISAModule_Sink<"+RISAModuleType+">*);\n"
			  "	dynStageDtor* destroy_stage = dlsym_s<dynStageDtor>(libHandle, (\"destroy_\" + name).c_str());\n"
			  "	dynStageCtor* create_stage = dlsym_s<dynStageCtor>(libHandle, (\"create_\" + name).c_str());\n"
			  "	stagesMap[identifier] = std::move(\n"
			  "		pipeline.create<\n"
			  "			glados::pipeline::SinkStage<DynamicallyLoadedSinkStage<dynStageType, dynStageCtor,dynStageDtor>>>(\n"
			  "			cfg_file, params, create_stage, destroy_stage));\n"
			  "}\n\n";

		BOOST_LOG_TRIVIAL(fatal)
			<< "RISAConfiguration: Requested input type '"<<type_in<<"' is currently not implemented.";
		BOOST_LOG_TRIVIAL(fatal) << "RISAConfiguration: Add the following code snippet to \n\n"
								 << __FILE__ << " at line " << addCodeInThisLine << ":\n"
								 << code;
		return false;
	}
	return true;
	// clang-format on
}
}

#endif /*RISA_MODULE_INTERFACE_HELPERS */
