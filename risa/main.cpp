// SPDX-FileCopyrightText: 2022 Helmholtz-Zentrum Dresden-Rossendorf
//
// SPDX-License-Identifier: Apache-2.0
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
// Authors:
// Dominic Windisch (d.windisch@hzdr.de),
// André Bieberle (a.bieberle@hzdr.de)

#define BOOST_STACKTRACE_USE_ADDR2LINE

#include "include/risa/RISAConfiguration.h"

#include <boost/date_time/posix_time/posix_time_io.hpp>
#include <boost/log/core.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/stacktrace.hpp>

#include <csignal>
#include <cstdlib>
#include <exception>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>
#include <thread>

#include <cuda.h>
#include <cuda_profiler_api.h>
#include <cuda_runtime.h>

// NOLINTNEXTLINE
std::string tmpPath;

void coloring_formatter(const boost::log::record_view& record, boost::log::formatting_ostream& stream)
{
	auto severity = record[boost::log::trivial::severity];
	assert(severity);

	switch(severity.get())
	{
	case boost::log::trivial::severity_level::trace:
		stream << "\033[97m";
		break;
	case boost::log::trivial::severity_level::debug:
		stream << "\033[30;1m";
		break;
	case boost::log::trivial::severity_level::info:
		stream << "\033[0m";
		break;
	case boost::log::trivial::severity_level::warning:
		stream << "\033[93m";
		break;
	case boost::log::trivial::severity_level::error:
		stream << "\033[91m";
		break;
	case boost::log::trivial::severity_level::fatal:
		stream << "\033[41m";
		break;
	}

	stream << "[" << boost::log::extract<boost::posix_time::ptime>("TimeStamp", record) << "] "
		   << "["
		   << boost::log::extract<boost::log::attributes::current_thread_id::value_type>("ThreadID", record)
		   << "] "
		   << "[" << severity << "] " << record[boost::log::expressions::message];

	if(severity)
	{
		stream << "\033[0m";
	}
}

void standard_formatter(const boost::log::record_view& record, boost::log::formatting_ostream& stream)
{
	auto severity = record[boost::log::trivial::severity];
	stream << "[" << boost::log::extract<boost::posix_time::ptime>("TimeStamp", record) << "] "
		   << "["
		   << boost::log::extract<boost::log::attributes::current_thread_id::value_type>("ThreadID", record)
		   << "] "
		   << "[" << severity << "] " << record[boost::log::expressions::message];
}

void initLog(std::string& logFilePath, bool useColoredOutput)
{
	boost::log::add_common_attributes();

	if(!logFilePath.empty())
	{
		boost::log::add_file_log(boost::log::keywords::file_name = logFilePath.c_str(),
			boost::log::keywords::auto_flush = true,
			boost::log::keywords::format = "[%TimeStamp%] [%Severity%]: %Message%");
	}
	auto consoleLog = boost::log::add_console_log(std::cout, boost::log::keywords::auto_flush = true);
	if(useColoredOutput)
	{
		consoleLog->set_formatter(&coloring_formatter);
	}
	else
	{
		consoleLog->set_formatter(&standard_formatter);
	}

#if !defined(RISA_LOG_LEVEL_INFO) && !defined(NDEBUG)
	boost::log::core::get()->set_filter(boost::log::trivial::severity >= boost::log::trivial::debug);
#else
	boost::log::core::get()->set_filter(boost::log::trivial::severity >= boost::log::trivial::info);
#endif
}

// NOLINTNEXTLINE
bool caughtSegfault = false;
static void signalHandler(int signum)
{
	if(signum == SIGSEGV && !caughtSegfault)
	{
		caughtSegfault = true;
		BOOST_LOG_TRIVIAL(fatal) << "Segmentation fault. Here is the backtrace: \n"
								 << boost::stacktrace::stacktrace();
		BOOST_LOG_TRIVIAL(info) << "Trying to exit gracefully...";
	}
	else if(signum == SIGSEGV && caughtSegfault)
	{
		BOOST_LOG_TRIVIAL(info) << "Unable to terminate gracefully. Forcing exit...";
		exit(EXIT_FAILURE);
	}
	const std::string tmpFile = tmpPath + "signalReceived.cfg";
	const std::string errString = "{\"triggerStop\":true}\n";
	BOOST_LOG_TRIVIAL(debug) << "Writing stop flag to " << tmpFile;
	// NOLINTBEGIN
#if __linux__
	int fh = open(tmpFile.c_str(), O_CREAT | O_WRONLY, 0666);
	if(write(fh, errString.c_str(), errString.size()) != (ssize_t)errString.size())
	{
		close(fh);
		throw std::runtime_error("Received a signal, unable to handle it");
	}
	close(fh);
#elif _WIN32
	std::ofstream fs(tmpFile);
	fs.write(errString.data(), errString.size());
	fs.close();
#endif
	// NOLINTEND
}

// NOLINTNEXTLINE(bugprone-exception-escape)
auto main(int argc, char* argv[]) -> int
{
	std::string logFilePath;
	bool useColoredOutput = true;
	for(int argi = 2; argi < argc; ++argi)
	{
		// NOLINTNEXTLINE
		const auto arg = std::string(argv[argi]);
		if(arg == "--disable-color")
		{
			useColoredOutput = false;
		}
		else
		{
			logFilePath = arg;
			std::cout << "Writing log output to file: " << logFilePath << std::endl;
		}
	}

	initLog(logFilePath, useColoredOutput);

	std::string setBold;
	std::string setColor;

	if(useColoredOutput)
	{
#ifdef _WIN32
		// colored output is not enabled by default on windows terminals
		HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
		DWORD dwMode = 0;
		GetConsoleMode(hOut, &dwMode);
		dwMode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING; // flag to enable colored output
		SetConsoleMode(hOut, dwMode);
#endif
		setColor = "\033[1;32m";
		setBold = "\033[0;1m";
	}

#ifndef NDEBUG
	const std::string binVers = "DEBUG    " + std::string(__DATE__) + "|" + std::string(__TIME__);
#else
	const std::string binVers = "RELEASE  " + std::string(__DATE__) + "|" + std::string(__TIME__);
#endif

	BOOST_LOG_TRIVIAL(info) << setBold << "#################################";
	BOOST_LOG_TRIVIAL(info) << setBold << "# " << setColor << R"( _____  _____  _____          )" << setBold << "#";
	BOOST_LOG_TRIVIAL(info) << setBold << "# " << setColor << R"(|  __ \|_   _|/ ____|  /\     )" << setBold << "#";
	BOOST_LOG_TRIVIAL(info) << setBold << "# " << setColor << R"(| |__) | | | | (___   /  \    )" << setBold << "#";
	BOOST_LOG_TRIVIAL(info) << setBold << "# " << setColor << R"(|  _  /  | |  \___ \ / /\ \   )" << setBold << "#";
	BOOST_LOG_TRIVIAL(info) << setBold << "# " << setColor << R"(| | \ \ _| |_ ____) / ____ \  )" << setBold << "#";
	BOOST_LOG_TRIVIAL(info) << setBold << "# " << setColor << R"(|_|  \_\_____|_____/_/    \_\ )" << setBold << "#";
	BOOST_LOG_TRIVIAL(info) << setBold << "#                               #";
	BOOST_LOG_TRIVIAL(info) << setBold << "#################################";
	BOOST_LOG_TRIVIAL(info) << setBold << "# " << binVers << " #";
	BOOST_LOG_TRIVIAL(info) << setBold << "#################################";

	if(argc < 2)
	{
		BOOST_LOG_TRIVIAL(fatal) << "Call the program like this: ./risa <path_to_config_file>";
		return EXIT_FAILURE;
	}

	// signal handling
	tmpPath = risa::setupTempPath();
	std::signal(SIGINT, signalHandler);
	std::signal(SIGTERM, signalHandler);
	std::signal(SIGSEGV, signalHandler);
	BOOST_LOG_TRIVIAL(debug) << "Using temporary path: " << tmpPath;

	std::string configFile{argv[1]}; // NOLINT
	BOOST_LOG_TRIVIAL(info) << "Starting RISA with configuration file: " << configFile;

	int numberOfDevices{0};
	auto ret = cudaGetDeviceCount(&numberOfDevices);
	if(ret != cudaSuccess)
	{
		BOOST_LOG_TRIVIAL(fatal) << "Cannot get device count: " << cudaGetErrorString(ret);
		return EXIT_FAILURE;
	}
	if(numberOfDevices == 0)
	{
		BOOST_LOG_TRIVIAL(fatal) << "No CUDA-capable devices found.";
		return EXIT_FAILURE;
	}

	for(int i = 0; i < numberOfDevices; i++)
	{
		cudaDeviceProp prop{};
		cudaGetDeviceProperties(&prop, i);
		BOOST_LOG_TRIVIAL(debug) << "GPU " << i << ": " << prop.name << " (CC " << prop.major << "."
								 << prop.minor << ")";
	}

	if(!std::filesystem::exists(configFile))
	{
		BOOST_LOG_TRIVIAL(fatal) << "Config file '" << configFile << "' does not exist.";
		return EXIT_FAILURE;
	}

	risa::ConfigReader configReader = risa::ConfigReader(configFile.data());
	std::string pipelineMode;
	if(!configReader.lookupValue("modeOfOperation", pipelineMode))
	{
		pipelineMode = "custom";
	}

	try
	{
		auto risa = risa::RISAConfiguration();

		if(!risa.generateConfiguration(configFile))
		{
			BOOST_LOG_TRIVIAL(fatal) << "Could not create pipeline configuration.";
			return EXIT_FAILURE;
		}

		BOOST_LOG_TRIVIAL(info) << "Pipeline configuration with mode \"" << pipelineMode << "\" initialized.";

		// run the configuration
		risa.startConfiguration();
		BOOST_LOG_TRIVIAL(info) << setColor << "Pipeline configuration with mode \"" << pipelineMode
								<< "\" running.";
		BOOST_LOG_TRIVIAL(info) << setBold << "Use Ctrl + C to stop the program.";
		for(auto i = 0; i < numberOfDevices; i++)
		{
			CHECK(cudaSetDevice(i));
			CHECK(cudaProfilerStart());
		}

		risa.waitUntilFinished();
		BOOST_LOG_TRIVIAL(info) << setColor << "Pipeline configuration with mode \"" << pipelineMode
								<< "\" stopped.";

		for(auto i = 0; i < numberOfDevices; i++)
		{
			CHECK(cudaSetDevice(i));
			CHECK(cudaProfilerStop());
		}
	}
	catch(const std::runtime_error& err)
	{
		std::cerr << "========================= " << std::endl;
		std::cerr << "A runtime error occurred: " << std::endl;
		std::cerr << err.what() << std::endl;
		std::cerr << "========================= " << std::endl;
	}

	for(auto i = 0; i < numberOfDevices; i++)
	{
		CHECK(cudaSetDevice(i));
		CHECK(cudaDeviceSynchronize());
		CHECK(cudaDeviceReset());
	}
	return EXIT_SUCCESS;
}
